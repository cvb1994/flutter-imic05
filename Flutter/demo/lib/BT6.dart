import 'dart:html';

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class Product {
  late int id;
  late String name;
  late String image;
  late double price;
  late String description;
  bool isFav = false;

  Product(int id, String name, String url, double price, String des) {
    this.id = id;
    this.name = name;
    this.image = url;
    this.price = price;
    this.description = des;
  }
}

List<Product> cart = <Product>[];
List<Product> myList = <Product>[
  Product(
      1,
      "Áo phông",
      "https://aothunvnxk.vn/wp-content/uploads/2015/09/ao-thun-nam-CO-TRON1.jpg",
      9.99,
      "Áo Phông"),
  Product(
      2,
      "Quần bò",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRYib5LQaRlpepz-adpl-Ek1qxN7p51mBaEw&usqp=CAU",
      15.00,
      "QUần bò"),
  Product(
      3,
      "Áo khoác",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4O67-_liRml2dUah68PSHAUQPASqNild45g&usqp=CAU",
      17.00,
      "Áo khoác gió"),
  Product(
      4,
      "Quần đùi",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToL2beHfycSYG1ggvT2wsflxa_NGEi1On7Nw&usqp=CAU",
      5.00,
      "QUần đùi nam")
];

void addData(Product value) {
  myList.add(value);
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: LoginScreen(),
    );
  }
}

enum FormType { login, register }

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  FormType _formType = FormType.login;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Material App Bar'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.purple.withAlpha(220),
                    Colors.orange.withAlpha(150)
                  ]),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Spacer(),
              Transform.rotate(
                angle: 3.14 / -15,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.red),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: Text(
                      'MyShop',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(30),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(width: 0.5, color: Colors.grey),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    const TextField(
                      decoration: InputDecoration(hintText: 'E-Mail'),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const TextField(
                      decoration: InputDecoration(hintText: 'Password'),
                    ),
                    AnimatedSize(
                        duration: const Duration(milliseconds: 200),
                        child: SizedBox(
                          height: _formType == FormType.login ? 0 : null,
                          child: const TextField(
                            decoration:
                                InputDecoration(hintText: 'Re-Password'),
                          ),
                        )),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (_formType == FormType.login) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ListProduct()));
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(color: Colors.red))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                            _formType == FormType.login ? 'Login' : 'Register'),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextButton(
                        onPressed: () {
                          setState(() {
                            if (_formType == FormType.login) {
                              _formType = FormType.register;
                            } else {
                              _formType = FormType.login;
                            }
                          });
                        },
                        child: Text(_formType == FormType.login
                            ? 'Register'
                            : 'Login')),
                  ],
                ),
              ),
              const Spacer(),
            ],
          )
        ],
      ),
    );
  }
}

class ListProduct extends StatefulWidget {
  const ListProduct({super.key});

  @override
  State<ListProduct> createState() => _ListProduct();
}

class _ListProduct extends State<ListProduct> {
  onGoBack(dynamic value) {
    setState(() {});
  }

  refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> list = <Widget>[];
    myList.forEach((element) => {
          // list.add(ListProductDetail(product: element,))
          list.add(GridProductDetail(
            product: element,
            function: refresh,
          ))
        });

    return Scaffold(
      appBar: AppBar(
        title: const Text("List Product"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8),
            child: Badge(
              label: Text(cart.length.toString()),
              alignment: AlignmentDirectional.topStart,
              child:
                  IconButton(onPressed: () {}, icon: Icon(Icons.shopping_cart)),
            ),
          ),
        ],
      ),
      body: GridView.count(
          padding: const EdgeInsets.all(10),
          childAspectRatio: 1.8,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          children: list),
    );
  }
}

class Detail extends StatefulWidget {
  const Detail({super.key, required this.product});
  final Product product;

  @override
  State<Detail> createState() => _Detail();
}

class _Detail extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: 400,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: Image.network(widget.product.image).image,
                        fit: BoxFit.cover)),
              ),
              // AppBar(
              //   backgroundColor: Colors.transparent,
              // ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(Icons.arrow_back),
                  color: Colors.white,
                ),
              ),
              Positioned(
                  bottom: 0,
                  left: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(8),
                    alignment: AlignmentDirectional.center,
                    height: 50,
                    child: Text(
                      widget.product.name,
                      style: const TextStyle(color: Colors.white, fontSize: 30),
                      textAlign: TextAlign.center,
                    ),
                  ))
            ],
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text(widget.product.price.toString() + "\$"),
          ),
          Container(
            child: Text(widget.product.description),
          ),
        ],
      ),
    );
  }
}

class GridProductDetail extends StatefulWidget {
  const GridProductDetail(
      {super.key, required this.product, required this.function});
  final Product product;
  final Function function;

  @override
  State<GridProductDetail> createState() => _GridProductDetail();
}

class _GridProductDetail extends State<GridProductDetail> {
  @override
  Widget build(BuildContext context) {
    void addCart() {
      cart.add(widget.product);
      widget.function.call();
    }

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        image: DecorationImage(
            image: Image.network(widget.product.image).image, fit: BoxFit.fill),
      ),
      child: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Detail(
                            product: widget.product,
                          )));
            },
          ),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: Container(
              decoration: const BoxDecoration(
                  color: Color.fromARGB(213, 28, 25, 25),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15))),
              height: 50,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          widget.product.isFav = !widget.product.isFav;
                        });
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          _buildIconFavorite(
                              28, Color.fromARGB(255, 199, 29, 190)),
                          _buildIconFavorite(
                              26,
                              widget.product.isFav
                                  ? Color.fromARGB(255, 199, 29, 190)
                                  : Color.fromARGB(249, 28, 25, 25)),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(widget.product.name,
                        style: const TextStyle(color: Colors.white)),
                  ),
                  Expanded(
                    child: IconButton(
                      icon: const Icon(Icons.shopping_cart,
                          color: Color.fromARGB(255, 199, 29, 190)),
                      highlightColor: Colors.red,
                      onPressed: () {
                        addCart();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildIconFavorite(double size, Color color) {
  return Container(
    height: 28,
    width: 28,
    alignment: Alignment.center,
    child: Icon(
      Icons.favorite,
      color: color,
      size: size,
    ),
  );
}
