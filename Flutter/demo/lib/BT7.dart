import 'dart:html';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(
  MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => ProductProvider()),
          ChangeNotifierProvider(create: (_) => CartProvider()),
          ChangeNotifierProvider(create: (_) => FavoriteProvider()),
        ],
        child: const MyApp(),
      ),
);

class Product {
  late int id;
  late String name;
  late String image;
  late double price;
  late String description;

  Product(this.id, this.name, this.image, this.price, this.description);
}

class ProductProvider extends ChangeNotifier{
  List<Product> myList = <Product>[
    Product(
        1,
        "Áo phông",
        "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDw0NDQ8PDQ0NDQ0NDw0NDQ8NDQ0NFREWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMuNygtLisBCgoKDQ0OGBAPFS0lHR8tLS0tKysrLS0tKy0tKy0tKy0tLS0rLS0rKystLS0rLSsrLS0rLS0rLS0tKy0tKystK//AABEIAQIAwwMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAAAQIDBwQGCAX/xABFEAACAgADBAUGCggFBQAAAAAAAQIDBBExBQYhQQcSUWFxEyIygZHBFCNCUoKho7Gy0SRiY2Ryc5KzFSUzdKI0Q1OT8P/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAHhEBAAIDAAMBAQAAAAAAAAAAAAECAxExEjJRQSH/2gAMAwEAAhEDEQA/AN0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEASAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACABIAAAAAAcLam1cPg63dibYVVr5U3lm+xLVvuXEDmnB2rtbDYOt24m2FMF8qcss32JayfcjWW8fStOXWr2dX1FxXwi9Zy8Y16Lxl7DXWPx1+KsduJtnfY/l2SzyXYuSXcuB0jHM9ZmzY+8PSxJt17OqSWnwjEJ5/Rr98n6jqGB6Rds03Tn8KdybTdV8ITqay5JJNfRaOvs42IWUoy5NdV+PI6eMRDO23NndMMcssVgmn8/DWqWf0JpZf1M+1T0r7JkvOWJr/AIqVL8MmaLRaKb04+CJ4VXct7y6UtjrSy99yw8/efNx3S9gop+Qw+Iuly6/k6YP15t/Uaaef/wBmVY8Km5d2270pbTxCcaHDBQ/Y+fbl/Mlp6kmX3a6Tto4WMI4hrHVcG1c3G5LusS/En6joF6z4atvJHKXDguSyL4x8TcvRO7W+mz9pZRpt8ne1xw12ULvo8p/RbOxHlTLno001lyfadz3c6SNo4Lqwtl8OoWS6l8n5aK/Vt1/q63qMTj+NRZvgHXN2N9MBtPKNM3XiMs3hrsoW8NerxymvB+OR2M5zGmgAEAAACCQAAAAApZPqrMDg7b2rVg6bL7ZdWFcXJ9r7Eu1t5JeJ5+3i25ftG+V97eWbVVWb6lVfKKXb2vmzsvSbvH8Ku+CVSzow8n12nwsv0fqjp459x0Zvid8ddf2WLSMqWZU6MhjnBSTT58+xmQqQceDa816r6+85mzXN21qux0zlJRVkXKLjn3riYJwUuD9T5oivylbUovjF5p8M0/B8CDse8sMRCEZSxErKpuuuUMpQjKUY8JatNeZpy4eJ1pyORisbiLko2PrJPrcY1x87tzSz5nHjT87j3LQRArTHN9d6fJ7+8zgkoAACYzcWpRbjKLTjKLcZRktGmtGb06M97ntKh0YiSeNw8V1no76tFb46J9+T5mh7XlkfS2FtW7BX1Yuh5WVS62T9GcNJQl3NZozau1idPTYOFsXalOOw9OKoedd0FJJ+lCWkoS7080/A5p53QAAAAAAABB0vpF3l+BUOFUssRfnCvLWC+VZ6uXe0do2rjoYeqy2ySjCuMpyk+UUjz5vHtezH4mzETzSk+rXB/wDbqXox9772zdK7lm06fMb5mKL4+0ySMUdV6zuwuypZlSiASCCGRkSCiuRORJBAAAEkEgDDf7jLW+Bju5F69EBsHoo3n+CXvBXSyw+Lmuo2+FWJ4JPwlwT71HvN1Hlg3z0b7z/4jhepbLPF4ZRhbnrZD5Fvryyfeu9HLJX9arP47cADk2AAAVnLJZljq2/O8KwGHlKLTun5lUXzm+b7lr9XMsRsdJ6Ut4/Kz+A1S8ytqV7Xyp6xh6tX35dhrxmSycpOUpNylJuUpPi5SbzbZjZ6axqNOUztSRiWq8TLIw5+cvEDIyGSyGVEEgEVBJBJRDBDCIJYDAQAAGKzl6y9ZSf5l4EVc+vuvtyzZuKqxVebUX1bK88lbS/Sh713pHyCSj1BgcXXiKq76ZKdVsI2QkucWvqfcZzUfRFvN5Ob2ZfL4u2Tnhm3whdrKvwlqu/P5xtw89o1LpE7AAZVx8biI1QlKTUVFOTk3kkks22aE3u25LaGJlbm/IwzhTF8oZ+ll2vX2Lkb02rgo31zrmlKE4uMovSUWuKZqXejcW3D9a3CKVlazbp1tgv1fnr6/E6Y5iJ/rNtukyKMtJFWd3NjmYE/Oj4nImcVenDxf3Mkq5DIJZUIkMgllEAACGEQSiCWEQwgJAAGGx5PLuL16GDGPKUO/gcitEVckIkqLVTlGUZRbjKLUoyTylGSeaafbmeg9x9447UwkbW0sRVlXiILhlZlwml2SXH2rkaW3Y3Wxe055UR6lUXlZiJp+Sh3L50u5evI3buputhdl1yjQnK2xR8rfN52W5Z5LsSWbyS7eepzyTDddvugA4thgvw0ZozgDXu9u49WK61tWVOI166XmWP9dL8S4+Jqraezb8JY6r4Oua0z9Ga7Yvmj0rOClwZ8Lbu79OKrddsFOD4rlKL+dF6pnSt5jrM1288TOPX6cfX9zO171bo4jAOU1ndhs+FqXnV9isS08dPDQpLc+7D4G3aGK61U8qVRQ1lNRlZFOdnZmm8o+3sOvlDOnX2ULyMZplJLIDAgkhEgVJiVJiQSwiGTECQgEBxsZHOVfi/uOTE+5uru9Dadl9Dk67K8NK6qxcVGxWQj5y5xylk0Y692Me8V8BVEniNeH+n5PP8A1evp1O/1a8DO42r5SWfBcW+CS4tvsNi7m9G1l/VxG0VKqrhKOG4xtsX7R/IXdr4HbdzNwcPs/q3XZYjF6+Ua+LpfZWnp/E+PhodzSMWyfGor9YcJha6IRqqhGuuCUYwhFRjFdiSM4BybAAAAAAhokAYvIRzzy4nRulR5YC7vnQvtYnfjXvSxL9Cs77aV/wAs/car2EnjTUzGXmUPQ5pRLIQYBElSxUUJgVYrIqzJgVZMAiwRBKA790MpPaNyejwF396k3PXVGOhpboaf+ZT78FevtKmbtOOTrpXgADm0AAAAAAAAAAAa56W5fob78RSvxM2KzWvS9L9Eiu3E1/gmar2EnjUE2VRMyp6HNKLSKR1LSYEZliiL5gUECGRDUC8hAiRENQMgRACO99Dry2n44S9fXB+43gaK6IZf5pWu3D4hf8c/cb1OOTrpXgADm0AAAAAAAAAACGaw6YZZYanvxcP7Vhs9mrOmN/EYdfvWf2U/zNU9oSeNTzKsmWpVnoc0wJkVgTICEZDGX5AUZENQyIagXkRDUmRWOoGUgZgDufRNLLa2H768Qvspfkb5NAdFsstr4Lv+EL7Cx+43+ccnW68AAc2gAACCSAJAAAAAQzVHTG/isMv3iT+zZteWhqTpkl5uEXbba/ZFfmap7JPGrXqVkSykj0Oa9YmKyJgQjIY4mRgY5CGoZEdQLyKrUtMpzQGUAMDtHRnLLa+z/wCZcvbh7EehTzr0cyy2ts/+fJe2qaPRRxydbqAA5tAAAEEgAAAAAAiWjNP9MsuOCX62JfsVf5m356M050yy8/BL/dP+0ap7JPGtWY2XZjZ3c2SrQiZavQrMoiOpkZiiZWBRkQJZFYF5mN6oySMUgM6ZDIiSwOwdH8stq7O/3MV7U17z0cea9x5ZbU2a/wB9w69s0veelDjk63UABzaAAAIAAkAAAABWejNL9Mj+OwS/VxP31m6LNGaV6Yn8fhP5d/4oG6dSeNeTMBmnJdq9piWXadmGaGhSwyZpc0UcHL0U5eCbCKwMsikFlrwfYy8ijGxXzIk12omprtRBdmKRlbWma9pinw107QMkCxSMl2r2llJdq9pR9bdGXV2js59mOwn96J6ZPMO7c0sdgHmv+uwfP9tA9PHHI3UABzaAAAAAAAAAABDWZxbMBXLi0n4pM5YA4D2VTzhD+lFls2pcFGPsRzQBwv8ADavmx/pRdYKCWSWS7FwOUAPn37Jps9OEZ/xxUvvOI918C3m8Lh2+10V/kfbAHy4bBwseEaal4VQXuKvd/CPWil+NMH7j6wA4FWyaIehXCP8ADCMfuMssFB8OXYcoAfPeyaXrCD8Yor/guH/8Vf8A64/kfSAHDw+zqa2nGuCa4pqEU0/YcwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAkAAAAAAAAAAQABIAAAACAAAJAAgAAAABIAA//Z",
        9.99,
        "Áo Phông"),
    Product(
        2,
        "Quần bò",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRYib5LQaRlpepz-adpl-Ek1qxN7p51mBaEw&usqp=CAU",
        15.00,
        "QUần bò"),
    Product(
        3,
        "Áo khoác",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4O67-_liRml2dUah68PSHAUQPASqNild45g&usqp=CAU",
        17.00,
        "Áo khoác gió"),
    Product(
        4,
        "Quần đùi",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToL2beHfycSYG1ggvT2wsflxa_NGEi1On7Nw&usqp=CAU",
        5.00,
        "QUần đùi nam")
  ];
}

class CartProvider extends ChangeNotifier{
  final cart = List<Product>.empty(growable: true);

  void addToCart(Product prod){
    cart.add(prod);
    notifyListeners();
  }
}

class FavoriteProvider extends ChangeNotifier{
  final fav = List<Product>.empty(growable: true);

  void addToFav(Product prod){
    fav.add(prod);
    notifyListeners();
  }

  void removeFromFav(Product prod){
    fav.remove(prod);
    notifyListeners();
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: LoginScreen(),
    );
  }
}

enum FormType { login, register }

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  FormType _formType = FormType.login;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Material App Bar'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.purple.withAlpha(220),
                    Colors.orange.withAlpha(150)
                  ]),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Spacer(),
              Transform.rotate(
                angle: 3.14 / -15,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.red),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: Text(
                      'MyShop',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(30),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(width: 0.5, color: Colors.grey),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    const TextField(
                      decoration: InputDecoration(hintText: 'E-Mail'),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const TextField(
                      decoration: InputDecoration(hintText: 'Password'),
                    ),
                    AnimatedSize(
                        duration: const Duration(milliseconds: 200),
                        child: SizedBox(
                          height: _formType == FormType.login ? 0 : null,
                          child: const TextField(
                            decoration:
                                InputDecoration(hintText: 'Re-Password'),
                          ),
                        )),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (_formType == FormType.login) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ListProduct()));
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(color: Colors.red))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                            _formType == FormType.login ? 'Login' : 'Register'),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextButton(
                        onPressed: () {
                          setState(() {
                            if (_formType == FormType.login) {
                              _formType = FormType.register;
                            } else {
                              _formType = FormType.login;
                            }
                          });
                        },
                        child: Text(_formType == FormType.login
                            ? 'Register'
                            : 'Login')),
                  ],
                ),
              ),
              const Spacer(),
            ],
          )
        ],
      ),
    );
  }
}

class ListProduct extends StatefulWidget {
  const ListProduct({super.key});

  @override
  State<ListProduct> createState() => _ListProduct();
}

class _ListProduct extends State<ListProduct> {
  @override
  Widget build(BuildContext context) {
    var productProvider = Provider.of<ProductProvider>(context);
    final products = productProvider.myList;

    return Scaffold(
      appBar: AppBar(
        title: const Text("List Product"),
        actions: <Widget>[
          Consumer<CartProvider>(
            builder: (context, value, child) {
              return Padding(
                padding: const EdgeInsets.all(8),
                child: Badge(
                  label: Text(value.cart.length.toString()),
                  alignment: AlignmentDirectional.topStart,
                  child:
                      IconButton(onPressed: () {}, icon: const Icon(Icons.shopping_cart)),
                ),
              );
            },
          )
        ],
      ),
      body: GridView.builder(
        itemCount: products.length,
        gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
        ), 
        itemBuilder: (context, index){
          return GridProductDetail(product: products.elementAt(index));
        })
    );
  }
}

class Detail extends StatefulWidget {
  const Detail({super.key, required this.product});
  final Product product;

  @override
  State<Detail> createState() => _Detail();
}

class _Detail extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: 400,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: Image.network(widget.product.image).image,
                        fit: BoxFit.cover)),
              ),
              // AppBar(
              //   backgroundColor: Colors.transparent,
              // ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(Icons.arrow_back),
                  color: Colors.white,
                ),
              ),
              Positioned(
                  bottom: 0,
                  left: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(8),
                    alignment: AlignmentDirectional.center,
                    height: 50,
                    child: Text(
                      widget.product.name,
                      style: const TextStyle(color: Colors.white, fontSize: 30),
                      textAlign: TextAlign.center,
                    ),
                  ))
            ],
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text(widget.product.price.toString() + "\$"),
          ),
          Container(
            child: Text(widget.product.description),
          ),
        ],
      ),
    );
  }
}

class GridProductDetail extends StatefulWidget {
  const GridProductDetail(
      {super.key, required this.product});
  final Product product;

  @override
  State<GridProductDetail> createState() => _GridProductDetail();
}

class _GridProductDetail extends State<GridProductDetail> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        image: DecorationImage(
            image: Image.network(widget.product.image).image, fit: BoxFit.fill
        ),
      ),
      child: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Detail(
                            product: widget.product,
                          )));
            },
          ),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: Container(
              decoration: const BoxDecoration(
                  color: Color.fromARGB(213, 28, 25, 25),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15))),
              height: 50,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        if(context.read<FavoriteProvider>().fav.contains(widget.product)){
                          context.read<FavoriteProvider>().removeFromFav(widget.product);
                        } else {
                          context.read<FavoriteProvider>().addToFav(widget.product);
                        }
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          _buildIconFavorite(
                                        28, Color.fromARGB(255, 199, 29, 190)),
                          Consumer<FavoriteProvider>(
                            builder: (context, value, child){
                              return  _buildIconFavorite(
                                        26,
                                        value.fav.contains(widget.product)
                                            ? Color.fromARGB(255, 199, 29, 190)
                                            : Color.fromARGB(249, 28, 25, 25));
                            }),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(widget.product.name,
                        style: const TextStyle(color: Colors.white)),
                  ),
                  Expanded(
                    child: IconButton(
                      icon: const Icon(Icons.shopping_cart,
                          color: Color.fromARGB(255, 199, 29, 190)),
                      highlightColor: Colors.red,
                      onPressed: () {
                        context.read<CartProvider>().addToCart(widget.product);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildIconFavorite(double size, Color color) {
  return Container(
    height: 28,
    width: 28,
    alignment: Alignment.center,
    child: Icon(
      Icons.favorite,
      color: color,
      size: size,
    ),
  );
}
