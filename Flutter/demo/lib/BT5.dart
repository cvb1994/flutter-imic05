import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class Product {
  late int id;
  late String name;
  late String image;
  bool isFav = false;

  Product(int id, String name, String url) {
    this.id = id;
    this.name = name;
    this.image = url;
  }
}

List<Product> myList = <Product>[
  Product(1, "Áo phông",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1EoUv9enpJ6Vrlf9fKutbq29zgd-xvqO02w&usqp=CAU"),
  Product(2, "Quần bò",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRYib5LQaRlpepz-adpl-Ek1qxN7p51mBaEw&usqp=CAU"),
  Product(3, "Áo khoác",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4O67-_liRml2dUah68PSHAUQPASqNild45g&usqp=CAU"),
  Product(4, "Quần đùi",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToL2beHfycSYG1ggvT2wsflxa_NGEi1On7Nw&usqp=CAU")
];

void addData(Product value) {
  myList.add(value);
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Material App',
      home: LoginScreen(),
    );
  }
}

enum FormType { login, register }

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  FormType _formType = FormType.login;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Material App Bar'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.purple.withAlpha(220),
                    Colors.orange.withAlpha(150)
                  ]),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Spacer(),
              Transform.rotate(
                angle: 3.14 / -15,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.red),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: Text(
                      'MyShop',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(30),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(width: 0.5, color: Colors.grey),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    const TextField(
                      decoration: InputDecoration(hintText: 'E-Mail'),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const TextField(
                      decoration: InputDecoration(hintText: 'Password'),
                    ),
                    AnimatedSize(
                        duration: const Duration(milliseconds: 200),
                        child: SizedBox(
                          height: _formType == FormType.login ? 0 : null,
                          child: const TextField(
                            decoration:
                                InputDecoration(hintText: 'Re-Password'),
                          ),
                        )),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (_formType == FormType.login) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ListProduct()));
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(color: Colors.red))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                            _formType == FormType.login ? 'Login' : 'Register'),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextButton(
                        onPressed: () {
                          setState(() {
                            if (_formType == FormType.login) {
                              _formType = FormType.register;
                            } else {
                              _formType = FormType.login;
                            }
                          });
                        },
                        child: Text(_formType == FormType.login
                            ? 'Register'
                            : 'Login')),
                  ],
                ),
              ),
              const Spacer(),
            ],
          )
        ],
      ),
    );
  }
}

class ListProduct extends StatefulWidget {
  const ListProduct({super.key});

  @override
  State<ListProduct> createState() => _ListProduct();
}

class _ListProduct extends State<ListProduct> {
  onGoBack(dynamic value) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> list = <Widget>[];
    myList.forEach((element) => {
          list.add(ListProductDetail(
            product: element,
          ))
        });

    return Scaffold(
      appBar: AppBar(
        title: const Text("List Product"),
        actions: <Widget>[
          Padding(
              padding: const EdgeInsetsDirectional.only(end: 30),
              child: Ink(
                decoration: const ShapeDecoration(
                  color: Color.fromARGB(255, 209, 3, 48),
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  icon: const Icon(Icons.add, color: Colors.white),
                  highlightColor: Colors.red,
                  onPressed: () {
                    Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const ProductDetail()))
                        .then(onGoBack);
                  },
                ),
              ))
        ],
      ),
      body: ListView(children: list),
    );
  }
}

class ListProductDetail extends StatefulWidget {
  const ListProductDetail({super.key, required this.product});
  final Product product;

  @override
  State<ListProductDetail> createState() => _ListProductDetail();
}

class _ListProductDetail extends State<ListProductDetail> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          border: Border.symmetric(
              vertical: BorderSide.none,
              horizontal: BorderSide(
                  color: Color.fromARGB(255, 214, 211, 211),
                  width: 0.5,
                  style: BorderStyle.solid))),
      child: Row(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.3,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              child: CircleAvatar(
                radius: 50, // Image radius
                backgroundImage: NetworkImage(widget.product.image),
              ),
            ),
          ),
          SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: Text(widget.product.name)),
          SizedBox(
              width: MediaQuery.of(context).size.width * 0.15,
              child: IconButton(
                icon: const Icon(Icons.edit,
                    color: Color.fromARGB(255, 197, 16, 16)),
                highlightColor: Color.fromARGB(255, 13, 182, 42),
                onPressed: () {},
              )),
          SizedBox(
              width: MediaQuery.of(context).size.width * 0.15,
              child: IconButton(
                icon: const Icon(Icons.delete,
                    color: Color.fromARGB(255, 202, 10, 10)),
                highlightColor: Color.fromARGB(255, 13, 182, 42),
                onPressed: () {},
              )),
        ],
      ),
    );
  }
}

class ProductDetail extends StatefulWidget {
  const ProductDetail({super.key, this.product});
  final Product? product;

  @override
  State<ProductDetail> createState() => _ProductDetail();
}

class _ProductDetail extends State<ProductDetail> {
  final _formKey = GlobalKey<FormState>();

  int? _id;
  String? _name;
  String? _image;

  void onPressedSubmit() {
    if (_formKey.currentState!.validate()) {
      Product newProd = Product(_id!, _name!, _image!);
      addData(newProd);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Product"),
        actions: <Widget>[
          Padding(
              padding: const EdgeInsetsDirectional.only(end: 30),
              child: Ink(
                decoration: const ShapeDecoration(
                  color: Color.fromARGB(255, 209, 3, 48),
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  icon: const Icon(Icons.save, color: Colors.white),
                  highlightColor: Colors.red,
                  onPressed: () {
                    onPressedSubmit();
                  },
                ),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'ID',
                  ),
                  onChanged: (String? value) {
                    setState(() {
                      _id = int.parse(value.toString());
                    });
                  },
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Vui lòng nhập ID';
                    } else {
                      try {
                        int.parse(value);
                      } catch (e) {
                        return 'Vui lòng nhập số';
                      }
                      return null;
                    }
                  },
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Tên Sản Phẩm',
                  ),
                  onChanged: (String? value) {
                    setState(() {
                      _name = value.toString();
                    });
                  },
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Vui lòng nhập tên sản phẩm';
                    } else {
                      return null;
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.25,
                        height: MediaQuery.of(context).size.width * 0.25,
                        child: Image.network(_image ?? ""),
                      ),
                      const Spacer(),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Ảnh',
                          ),
                          onChanged: (String? value) {
                            setState(() {
                              _image = value.toString();
                            });
                          },
                          validator: (String? value) {
                            if (value!.isEmpty) {
                              return 'Vui lòng nhập url ảnh sản phẩm';
                            } else {
                              return null;
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
