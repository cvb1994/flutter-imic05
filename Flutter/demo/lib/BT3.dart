import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue, scaffoldBackgroundColor: Colors.white),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> names = ['Class 1', 'Class 2', 'Class 3', 'Class 4', 'Class 5'];
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final queryData = MediaQuery.of(context);
    final isLargeScreen = queryData.size.width > 900;
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: isLargeScreen ? Row(
        children: [
          SizedBox(
            width: 300,
            child: ListView(
              padding: const EdgeInsets.all(8),
              children: List.generate(names.length, (index) {
                return ListTile(
                  title: Text(names.elementAt(index)),
                  onTap: () => _onItemTapped(index),
                );
              }),
            ),
          ),
          Center(
            child: Text(names.elementAt(selectedIndex)),
          ),
        ],
      ) : 
      Center(
        child: ListView(
              padding: const EdgeInsets.all(8),
              children: List.generate(names.length, (index) {
                return ListTile(
                  title: Text(names.elementAt(index)),
                  onTap: () => _onItemTappedSmall(index),
                );
              }),
            ),
      )
      
      
    );
  }

  void _onItemTapped(int value) {
    setState(() {
      selectedIndex = value;
    });
  }

  void _onItemTappedSmall(int value) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Detail(title: names.elementAt(value))));
  }
}

class Detail extends StatefulWidget {
  const Detail({super.key, required this.title});
  final String title;

  @override
  State<Detail> createState() => _Detail();
}

class _Detail extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text(widget.title),)
    );
  }
}